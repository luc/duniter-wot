#!/usr/bin/perl
use Mojo::Base -base;
use Mojo::UserAgent;
use Mojo::File;
use Mojo::JSON qw(encode_json decode_json true);
use Mojo::IOLoop;
use Mojo::Collection 'c';
use Term::ProgressBar;

my $use_cache = ($ENV{USE_CACHE}) ? 1 : 0;
my $use_bar   = ($ENV{NO_BAR})    ? 0 : 1;

mkdir 'cache' unless -d 'cache';
my $ua = Mojo::UserAgent->new();
my $node = 'https://g1.duniter.fr';
$ua->get($node.'/wot/members' => sub {
        my ($ua, $tx) = @_;
        my $res = $tx->result;
        if ($res->is_success) {
            my $members = c(@{decode_json($res->body)->{results}});
            my %nodes = ();
            my @edges = ();
            my $bar = Term::ProgressBar->new($members->size) if $use_bar;
            $members->each(
                sub {
                    my ($m, $num) = @_;
                    $nodes{$m->{pubkey}} = {
                        id    => $m->{pubkey},
                        label => $m->{uid},
                        mass  => 5,
                        color => {
                            background => 'plum',
                            highlight  => {
                                background => 'lightgreen',
                                border     => 'green',
                            }
                        }
                    };

                    my $certs;
                    if (-e 'cache/'.$m->{pubkey} && $use_cache) {
                        $certs = c(@{decode_json(Mojo::File->new('cache/'.$m->{pubkey})->slurp)->{identities}->[0]->{certifications}});
                    } else {
                        my $tx = $ua->get($node.'/wot/requirements/'.$m->{pubkey});
                        my $res = $tx->result;
                        if ($res->is_success) {
                            $certs = c(@{decode_json($res->body)->{identities}->[0]->{certifications}});
                        } elsif ($res->is_error) {
                            say $res->message;
                        }
                        Mojo::File->new('cache/'.$m->{pubkey})->spurt($res->body);
                        sleep 2;
                    }
                    if (defined $certs) {
                        $certs->each(
                            sub {
                                my ($e, $n) = @_;
                                push @edges, {
                                    from   => $e->{from},
                                    to     => $e->{to},
                                    width  => 3,
                                    arrows => {
                                        to => {
                                            enabled => true
                                        }
                                    },
                                    timestamp => $e->{timestamp},
                                    color => {
                                        inherit => 'both',
                                    }
                                };
                            }
                        );
                    }
                    $bar->update($num) if $use_bar;
                }
            );
            my $tx = $ua->get($node.'/wot/pending');
            my $res = $tx->result;
            if ($res->is_success) {
                my $members = c(@{decode_json($res->body)->{memberships}});
                my $bar = Term::ProgressBar->new($members->size) if $use_bar;
                $members->each(
                    sub {
                        my ($m, $num) = @_;
                        unless (defined($nodes{$m->{pubkey}})) {
                            $nodes{$m->{pubkey}} = {
                                id    => $m->{pubkey},
                                label => $m->{uid},
                                mass  => 5,
                                color => {
                                    background => 'plum',
                                    highlight  => {
                                        background => 'lightgreen',
                                        border     => 'green',
                                    }
                                }
                            };

                            my $certs;
                            if (-e 'cache/'.$m->{pubkey} && $use_cache) {
                                $certs = c(@{decode_json(Mojo::File->new('cache/'.$m->{pubkey})->slurp)->{identities}->[0]->{certifications}});
                            } else {
                                my $tx = $ua->get($node.'/wot/requirements/'.$m->{pubkey});
                                my $res = $tx->result;
                                if ($res->is_success) {
                                    $certs = c(@{decode_json($res->body)->{identities}->[0]->{certifications}});
                                } elsif ($res->is_error) {
                                    say $res->message;
                                }
                                Mojo::File->new('cache/'.$m->{pubkey})->spurt($res->body);
                                sleep 2;
                            }
                            if (defined $certs) {
                                $certs->each(
                                    sub {
                                        my ($e, $n) = @_;
                                        push @edges, {
                                            from   => $e->{from},
                                            to     => $e->{to},
                                            width  => 3,
                                            arrows => {
                                                to => {
                                                    enabled => true
                                                }
                                            },
                                            timestamp => $e->{timestamp},
                                            color => {
                                                inherit => 'both',
                                            }
                                        };
                                    }
                                );
                            }
                        }
                        $bar->update($num) if $use_bar;
                    }
                );
            } elsif ($res->is_error) {
                say $res->message;
            }
            @edges = sort {$a->{timestamp} <=> $b->{timestamp}} @edges;
            map { delete $_->{timestamp} } @edges;
            my $data_nodes = encode_json(\%nodes);
            my $data_edges = encode_json(\@edges);
            my $page = <<EOF;
<!DOCTYPE html>
<html>
    <head>
        <title>Duniter : toile de confiance</title>
        <meta charset="utf-8">
        <link href="vis-network.min.css" rel="stylesheet">
        <style>
            #mynetwork {
              width: 98vw;
              height: 98vh;
            }
            #w3-border {
              border: 1px solid #ccc !important;
              width: 100%;
            }
            #w3-progress-bar {
              color: #000 !important;
              background-color: green !important;
              height: 24px;
              width: 0%;
            }
            #w3-progress-text {
              text-align: center;
              margin-top: -24px;
              height: 24px;
            }
        </style>
        <script src="vis.js"></script>
    </head>
    <body>
        <div id="w3-border">
            <div id="w3-progress-bar"></div>
            <div id="w3-progress-text">0%</div>
        </div>
        <div id="mynetwork"></div>
        <script>
            var n     = $data_nodes;
            var e     = $data_edges;
            var ne    = {};
            var nodes = new vis.DataSet([]);
            var edges = new vis.DataSet([]);
            var container = document.getElementById('mynetwork');
            var total = e.length;
            var data = {
                nodes: nodes,
                edges: edges
            };
            var options = {
                layout: {
                    improvedLayout: false,
                },
                physics: {
                    stabilization: {
                        enabled: false
                    },
                    timestep: 0.1
                }
            };
            var network = new vis.Network(container, data, options);
            console.info('Number of edges: '+e.length);
            function addEdge(ed) {
                setTimeout(function() {
                    var edg = ed.shift();
                    var w = 100 * (total - ed.length)/total;
                    document.getElementById('w3-progress-bar').style.width = w+'%';
                    var w2 = Math.round(100000 * (total - ed.length)/total) / 1000;
                    document.getElementById('w3-progress-text').textContent = w2+'%';
                    console.info('Adding edge');
                    if (edg.from !== undefined && n[edg.from] !== undefined && ne[edg.from] === undefined) {
                        nodes.add(n[edg.from]);
                        ne[edg.from] = 1;
                    }
                    if (edg.to !== undefined && n[edg.to] !== undefined && ne[edg.to] === undefined) {
                        nodes.add(n[edg.to]);
                        ne[edg.to] = 1;
                    }
                    edges.add(edg);
                    if (ed.length !== 0) {
                        addEdge(ed);
                    }
                }, 250);
            }
            addEdge(e);
        </script>
    </body>
</html>
EOF

             Mojo::File->new('public/index.html')->spurt($page);
        } elsif ($res->is_error) {
            say $res->message;
        }
    }
);

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;
